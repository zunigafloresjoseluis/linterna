// granjero001.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>


using namespace std;

class dato {
private:
	char granjero;
	char acom;
public:
	void setDato(char i[]) {
		granjero = i[0];
		acom = i[1];
	}
	char getGranjero() {
		return granjero;
	}
	char getAcompanante() {
		return acom;
	}
};

int buscar(char a, char lado[]);
int es_solucion(char lado[]);
void asignar(char aux_lado[], char lado[]);
void cruzar(int profundidad, char ladoA[], char ladoB[], int lado, char estados[]);
void imprimir_pila(stack<dato> pila);
void imprimir_lado(char lado[]);
//
char lado_A[] = { 'G','L','O','R' };
char lado_B[] = { ' ',' ',' ',' ' };

stack<dato> pila;


int main() {
	char e[2];
	e[0] = ' ';
	e[1] = ' ';
	cruzar(0, lado_A, lado_B, 0, e);
	return 1;

}

void cruzar(int profundidad,char ladoA[],char ladoB[],int lado,char estados[]) {
	if (profundidad <=7) {
		dato aux;
		aux.setDato(estados);
		pila.push(aux);
		if (es_solucion(ladoB) == 1) {
			printf("solucion!!");
			printf("\n");
			imprimir_pila(pila);
			printf("\n");
			return;
		}
		else {
			if (lado == 0) {
				//char lado_A[] = { 'G','L','O','R' };
			/*
				if ((ladoA[1] == ' ' || ladoA[2] == ' ') && (ladoA[2] == ' ' || ladoA[3] == ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);

					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					
					datos[0] = aux_ladoA[0];
					datos[1] = aux_ladoA[0];

					aux_ladoA[0] = val;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 1, datos);
				}
				*/
				if (((ladoA[2] == ' ' || ladoA[3] == ' ')||(ladoA[2]!=' '&&ladoA[3]==' ') || ((ladoA[2] == ' '&&ladoA[3] != ' '))) && (ladoA[1] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];

					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];

					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;

					char val2 = aux_ladoB[1];
					aux_ladoB[1] = aux_ladoA[1];
					datos[1] = aux_ladoA[1];
					aux_ladoA[1] = val2;

					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 1, datos);
				}
				if (ladoA[2] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];

					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					
					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;

					char val2 = aux_ladoB[2];
					aux_ladoB[2] = aux_ladoA[2];
					datos[1] = aux_ladoA[2];
					aux_ladoA[2] = val2;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 1, datos);
				}
				if (((ladoA[1] == ' ' || ladoA[2] == ' ')|| (ladoA[1] != ' ' || ladoA[2] == ' ')|| (ladoA[1] == ' ' || ladoA[2] != ' ')) && ladoA[3] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;
					char val2 = aux_ladoB[3];
					aux_ladoB[3] = aux_ladoA[3];
					datos[1] = aux_ladoA[3];
					aux_ladoA[3] = val2;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 1, datos);
				}
			
			}
			else {
				
				if ((ladoB[1] == ' ' || ladoB[2] == ' ') && (ladoB[2] == ' ' || ladoB[3] == ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);

					char datos[2];
					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];

					datos[0] = aux_ladoB[0];
					datos[1] = aux_ladoB[0];

					aux_ladoB[0] = val;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);
				}
				
				if (((ladoB[2] == ' ' || ladoB[3] == ' ') || (ladoB[2] != ' '&&ladoB[3] == ' ') || ((ladoB[2] == ' '&&ladoA[3] != ' '))) && (ladoB[1] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];

					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];

					datos[0] = aux_ladoB[0];
					aux_ladoB[0] = val;

					char val2 = aux_ladoA[1];
					aux_ladoA[1] = aux_ladoB[1];
					datos[1] = aux_ladoB[1];
					aux_ladoB[1] = val2;

					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);
				}
				
				if (ladoB[2] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];

					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];

					datos[0] = aux_ladoB[0];
					aux_ladoB[0] = val;

					char val2 = aux_ladoA[2];
					aux_ladoA[2] = aux_ladoB[2];
					datos[1] = aux_ladoB[2];
					aux_ladoB[2] = val2;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);
				}
				
				if (((ladoB[1] == ' ' || ladoB[2] == ' ') || (ladoB[1] != ' ' || ladoB[2] == ' ') || (ladoB[1] == ' ' || ladoB[2] != ' ')) && ladoB[3] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];
					datos[0] = aux_ladoB[0];
					aux_ladoA[0] = val;
					char val2 = aux_ladoA[3];
					aux_ladoA[3] = aux_ladoB[3];
					datos[1] = aux_ladoB[3];
					aux_ladoB[3] = val2;
					cruzar(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);
				}

			
			}


			pila.pop();
		
		
		}


	
	}


}



int es_solucion(char lado[]) {
	int contador = 0;

	for (int i = 0; i < 4; i++) {
		if (buscar(lado[i], lado_A) == 0) {
			contador++;
		}

	}
	if (contador > 0) {
		return 0;
	}
	return 1;

}
int buscar(char a, char lado[]) {
	for (int i = 0; i <4; i++) {
		if (lado[i] == a) {
			return 1;
		}
	}

	return 0;
}

void asignar(char aux_lado[], char lado[]) {
	for (int i = 0; i < 4; i++) {
		aux_lado[i] = lado[i];
	}


}

void imprimir_pila(stack<dato> pila) {
	stack<dato>aux = pila;
	int tam = aux.size() - 1;
	while (!aux.empty()) {
		printf(" *--> %c  %c", aux.top().getGranjero(), aux.top().getAcompanante());
		aux.pop();
	}
}
void imprimir_lado(char lado[]) {
	for (int i = 0; i < 4; i++) {
		printf(" %c ", lado[i]);
	}
}
