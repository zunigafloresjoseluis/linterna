// oveja.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>


using namespace std;


int buscar(char a, char lado[]);
int es_solucion(char lado[]);
void asignar(char aux_lado[], char lado[]);
void imprimir_lado(char lado[]);
void encontrar_camino(int profundidad, char ladoA[], char ladoB[], int lado,char estado[]);

class dato {
private :
	char granjero;
	char acom;
public:
	void setDato(char i[]) {
		granjero = i[0];
		acom = i[1];
	}
	char getGranjero() {
		return granjero;
	}
	char getAcompanante() {
		return acom;
	}
};
char lado_A[] = { 'G','L','O','R' };
char lado_B[] = { ' ',' ',' ',' ' };

void imprimir_pila(stack<dato> pila);
void imprimir_otra(stack<char>p);
stack<dato> pila;
stack<char> p;

int main(){
	char estados[2];
	estados[0] = ' ';
	estados[1] = ' ';
	
	encontrar_camino(0, lado_A, lado_B, 0,estados);

	


	return 1;
}

int es_solucion(char lado[]) {
	int contador = 0;

	for (int i = 0; i < 4; i++) {
		if (buscar(lado[i], lado_A) == 0) {
			contador++;
		}

	}
	if (contador > 0) {
		return 0;
	}
	return 1;

}
int buscar(char a, char lado[]) {
	for (int i = 0; i <4; i++) {
		if (lado[i] == a) {
			return 1;
		}
	}

	return 0;
}
void asignar(char aux_lado[], char lado[]) {
	for (int i = 0; i < 4; i++) {
		aux_lado[i] = lado[i];
	}


}
void imprimir_lado(char lado[]) {
	for (int i = 0; i < 4; i++) {
		printf(" %c ", lado[i]);
	}
}

void encontrar_camino(int profundidad,char ladoA[],char ladoB[],int lado,char estado[]) {
	if (profundidad <= 10) {
		dato aux;
		aux.setDato(estado);

		pila.push(aux);
		if (es_solucion(ladoB) == 1) {
			printf("solucion encontrada \n");
			imprimir_lado(ladoB);
			printf("\n");
			imprimir_pila(pila);
			printf("\n");
			return;
		}
		else {
			
			if (lado==0) {
				//char ladoA[] = { 0'G',1'L',2'O',3'R' };
			   

				//G-l
				if ((ladoA[2] == ' ' || ladoA[3] == ' ') && (ladoA[1] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];

					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;
					char val2 = aux_ladoB[1];
					aux_ladoB[1] = aux_ladoA[1];
					datos[1] = aux_ladoA[1];
					aux_ladoA[1] = val2;
					
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 1,datos);
				
				}
				//G-O
				if (ladoA[2] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					datos[0] = aux_ladoA[0];

					aux_ladoA[0] = val;
					char val2 = aux_ladoB[2];
					aux_ladoB[2] = aux_ladoA[2];

					datos[1] = aux_ladoA[2];
					aux_ladoA[2] = val2;
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 1,datos);
				
				}
				//G- R
				if ((ladoA[1] == ' ' || ladoA[2] != ' ') && (ladoA[3] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;
					char val2 = aux_ladoB[3];
					aux_ladoB[3] = aux_ladoA[3];
					datos[1] = aux_ladoA[3];
					aux_ladoA[3] = val2;
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 1,datos);

				
				
				}





			}
			else {
				//char ladoA[] = { 0'G',1'L',2'O',3'R' };



				if (((ladoB[1] != ' ' && ladoB[2] == ' ') || (ladoB[2] != ' ' && ladoB[3] == ' ')) || ((ladoB[1] == ' ' && ladoB[2] != ' ') || (ladoB[2] == ' ' && ladoB[3] != ' ')) || ((ladoB[1] == ' ' && ladoB[2] == ' ') || (ladoB[2] == ' ' && ladoB[3] == ' '))) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);

					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoA[0] = aux_ladoB[0];
					datos[0] = aux_ladoB[0];
					datos[1] = aux_ladoB[0];
					aux_ladoB[0] = val;

					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);
				}


				if (ladoB[2] != ' ') {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoB[0];
					aux_ladoB[0] = aux_ladoA[0];
					datos[0] = aux_ladoA[0];
					aux_ladoA[0] = val;

					char val2 = aux_ladoA[2];
					aux_ladoA[2] = aux_ladoB[2];
					datos[1] = aux_ladoB[2];
					aux_ladoB[2] = val2;
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 0, datos);

				}
				//G-l
				
				if ((ladoB[2] == ' ' || ladoB[3] == ' ') && (ladoB[1] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];
					datos[0] = aux_ladoB[0];
					aux_ladoB[0] = val;

					char val2 = aux_ladoA[1];
					aux_ladoA[1] = aux_ladoB[1];
					datos[1] = aux_ladoB[1];
					aux_ladoB[1] = val2;
				//	printf("\n* %c %c \n", datos[0], datos[1]);
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 0,datos);

				}
				//G-O
			
				//G- R
				if ((ladoB[1] == ' ' || ladoB[2] != ' ') && (ladoB[3] != ' ')) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					char datos[2];
					char val = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];
					datos[0] = aux_ladoB[0];
					aux_ladoB[0] = val;
					char val2 = aux_ladoA[3];
					aux_ladoA[3] = aux_ladoB[3];
					datos[1]= aux_ladoB[3];
					aux_ladoB[3] = val2;
					encontrar_camino(profundidad + 1, aux_ladoA, aux_ladoB, 0,datos);



				}

				
			
			
			}
			pila.pop();
		
		
		}
	
	
	}
		
	



}

void imprimir_pila(stack<dato> pila) {
	stack<dato>aux = pila;
	int tam = aux.size() - 1;
	while(!aux.empty()){
		printf(" *--> %c  %c", aux.top().getGranjero(), aux.top().getAcompanante());
		aux.pop();
	}
}

void imprimir_otra(stack<char>p) {
	stack<char> aux = p;
	while (!aux.empty()) {
		printf(" -%c ", aux.top());
		aux.pop();
	}

}
