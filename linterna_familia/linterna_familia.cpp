// linterna_familia.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>
using namespace std;
int buscar_miembro(int integrante, int familia[]);
int es_solucion(int ladoB[], int familia[]);
void asignar(int a[], int b[]);
void no_pasados(int a[], int b[], int c[]);
void limpiar_pila(stack<int> p);
void imprimir_pila(stack<int> pila);
void linterna(int profundidad, int familia[], int ladoB[], int lado,int suma,stack<int>pila);
void imprimir_familia(int familia[]);
int suma_familia(int familia[]);
int suma_pila(stack<int> pil);
int solucion_pila(stack<int> pila);
int busca_pila(stack<int> pila, int numero);
int familia_v[] = { 1,3,6,8,12 };
int ladoB[] = { 0,0,0,0,0 };//{ 0,0,0,0,0 };
int main() {
	stack<int> pila;
	
	linterna(0, familia_v, ladoB, 0,0,pila);
	
	return 1;
}

void linterna(int profundidad, int familia[], int ladoB[], int lado, int suma, stack<int> pila) {


	if (profundidad <= 6) {
		
		
			if (lado == 0) {
				for (int i = 0; i < 5; i++) {
					if (familia[i] != 0) {
						int arr_no_pasados[] = { 0,0,0,0,0 };
						no_pasados(familia, ladoB, arr_no_pasados);

						for (int j = 0; j < 5; j++) {
							if (arr_no_pasados[j] != 0  && familia[i] != arr_no_pasados[j]) {
								int aux_familia[5];
								int aux_ladoB[5];
								asignar(aux_familia, familia);
								asignar(aux_ladoB, ladoB);
								int nueva_suma = suma;
								stack<int> nuevo = pila;
								nuevo.push(familia[i]);
								nuevo.push(arr_no_pasados[j]);
								if (familia[i] > arr_no_pasados[j]) {
									nueva_suma += familia[i];
								}
								else {
									nueva_suma += arr_no_pasados[j];
								}

								int valor_ladoB = aux_ladoB[i];
								aux_ladoB[i] = familia[i];
								aux_familia[i] = valor_ladoB;
								aux_ladoB[j] = arr_no_pasados[j];
								aux_familia[j] = 0;
								linterna(profundidad + 1, aux_familia, aux_ladoB, 1, nueva_suma, nuevo);

							}
						}

					}
				}

			}
			else {
				for (int k = 0; k < 5; k++) {
					if (ladoB[k] != 0) {

						int aux_familia[5];
						int aux_ladoB[5];
						asignar(aux_familia, familia);
						asignar(aux_ladoB, ladoB);
						stack<int> nuevo = pila;
						int valor = aux_familia[k];
						aux_familia[k] = aux_ladoB[k];
						nuevo.push(aux_ladoB[k]);
						int nueva_suma = suma;
						nueva_suma += aux_ladoB[k];
						aux_ladoB[k] = valor;

						linterna(profundidad + 1, aux_familia, aux_ladoB, 0, nueva_suma, nuevo);
					}
				}

			}

		

	}

	if (es_solucion(ladoB, familia_v)==1 && suma<31 && suma>27) {
		//imprimir_pila(pila);
		printf("\n----------> encontrada");
		printf("\n");
		imprimir_familia(familia);
		printf("\n");
		imprimir_familia(ladoB);
		printf("\n");
		printf("----> imprimiendo \n");
		imprimir_pila(pila);
		return;
	}
	//imprimir_pila(pila);
//	printf("----> suma %d\n",suma);
	
	
	

}


void limpiar_pila(stack<int> p) {
	stack<int> aux = p;
	while (!aux.empty())
	{
		aux.pop();
	}
}

int es_solucion(int ladoB[],int familia[]) {
	int contador = 0;
	for (int i = 0; i < 5; i++) {
		if (buscar_miembro(ladoB[i], familia) == 0) {
			contador++;
		    
		}
	
	}
	
	if (contador > 0) {
		return 0;
	}
	return 1;
}

int buscar_miembro(int integrante,int familia[]) {
	for (int i = 0; i < 5; i++) {
		if (integrante == familia[i]) {
			return 1;
		}
	}
	
	return 0;
}

void asignar(int a[],int b[]) {
	for (int i = 0; i < 5; i++) {
		a[i] = b[i];
	}

}

void imprimir_familia(int familia[]) {
	for (int i = 0; i < 5; i++) {
		printf(" %d ", familia[i]);
	}


}


int suma_familia(int familia[]) {
	int suma = 0;
	for (int i = 0; i < 5; i++) {
		suma += familia[i];
	}
	return suma;

}

int suma_pila(stack<int> pil) {
	stack<int> a = pil;
	int suma=0;
	while (!a.empty()) {
		suma += a.top();
		a.pop();
	}
	return suma;
}


void imprimir_pila(stack<int> pila) {
	stack<int> aux = pila;
	while (!aux.empty())
	{
		printf(" %d  ", aux.top());
		aux.pop();
	}
}

void no_pasados(int a[], int b[], int c[]) {
	
	for (int i = 0; i < 5; i++) {
		if (buscar_miembro(a[i], b) == 0) {
			c[i] = a[i];
		}
	}


}

int solucion_pila(stack<int> pila) {

	int contador = 0;
	int familia_w[] = { 1,3,6,8,12 };
	for (int i = 0; i < 5; i++) {
		if (busca_pila(pila, familia_w[i]) == 0) {
			contador++;
		}

	}
	if (contador > 0) {
	
		return 0;
	}
	return 1;
}

int busca_pila(stack<int> pila, int numero) {
	stack<int> aux = pila;
	
	while (!aux.empty()) {
		if (aux.top() == numero) {
			return 1;
		}
		aux.pop();
	
	}

	return 0;
}