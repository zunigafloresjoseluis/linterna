
void pasa_granjero(int profundidad,char ladoA[],char ladoB[],int lado,stack<char>pila) {
	if (profundidad <= 6) {
		if (lado == 0) {
			for (int i = 0; i < 5; i++) {
				
				
					if (ladoA[i] != ' ') {
						char aux_ladoA[5];
						char aux_ladoB[5];
						asignar(aux_ladoA, ladoA);
						asignar(aux_ladoB, ladoB);
						stack<char>aux = pila;
						aux.push(aux_ladoA[0]);
						char val = aux_ladoB[0];
						aux_ladoB[0] = aux_ladoA[0];
						aux_ladoA[0] = val;
						for (int j = 0; j < 5; j++) {
							if (ladoA[j] != ' ') {
								if (ladoA[j] == 'R'&& ladoB[3]!='O') {
									char valor = aux_ladoB[j];
									aux_ladoB[j] = aux_ladoA[j];
									aux.push(aux_ladoA[j]);
									aux_ladoA[j] = valor;
									pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);
								}
								if (ladoA[j] == 'L' && ladoB[3] != 'O') {
									char valor = aux_ladoB[j];
									aux_ladoB[j] = aux_ladoA[j];
									aux.push(aux_ladoA[j]);
									aux_ladoA[j] = valor;
									pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);

								}
								if (ladoA[j] == 'O' && ladoB[1] != 'R') {
									char valor = aux_ladoB[j];
									aux_ladoB[j] = aux_ladoA[j];
									aux.push(aux_ladoA[j]);
									aux_ladoA[j] = valor;
									pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);
								
								}
								if (ladoA[j] == 'O' && ladoB[2] != 'L') {
									char valor = aux_ladoB[j];
									aux_ladoB[j] = aux_ladoA[j];
									aux.push(aux_ladoA[j]);
									aux_ladoA[j] = valor;
									pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);

								
								}


							
							}
						}
					}
			}
		}
		else {
			for (int i = 0; i < 5; i++) {
				char aux_ladoA[5];
				char aux_ladoB[5];
				asignar(aux_ladoA, ladoA);
				asignar(aux_ladoB, ladoB);
				stack<char>aux = pila;
				
				if (aux_ladoB[i] != ' ') {
					
					if (aux_ladoB[i] == 'R' && aux_ladoA[3] != 'O') {
						char val = aux_ladoA[0];
						aux_ladoA[0] = aux_ladoB[0];
						aux.push(aux_ladoB[0]);
						aux_ladoB[0] = val;



						char valor = aux_ladoA[i];
						aux_ladoA[i] = aux_ladoB[i];
						aux.push(aux_ladoB[i]);
						aux_ladoB[i] = valor;
						pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);

					}
					if (aux_ladoB[i] == 'L' && aux_ladoA[3] != 'O') {
						char val = aux_ladoA[0];
						aux_ladoA[0] = aux_ladoB[0];
						aux.push(aux_ladoB[0]);
						aux_ladoB[0] = val;



						char valor = aux_ladoA[i];
						aux_ladoA[i] = aux_ladoB[i];
						aux.push(aux_ladoB[i]);
						aux_ladoB[i] = valor;
						pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);
					
					}
					if (aux_ladoB[i] == 'O' && aux_ladoA[1] != 'R') {
						char val = aux_ladoA[0];
						aux_ladoA[0] = aux_ladoB[0];
						aux.push(aux_ladoB[0]);
						aux_ladoB[0] = val;
						
						char valor = aux_ladoA[i];
						aux_ladoA[i] = aux_ladoB[i];
						aux.push(aux_ladoB[i]);
						aux_ladoB[i] = valor;
						pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);
					}
					if (aux_ladoB[i] == 'O' && aux_ladoA[2] != 'L') {
						char val = aux_ladoA[0];
						aux_ladoA[0] = aux_ladoB[0];
						aux.push(aux_ladoB[0]);
						aux_ladoB[0] = val;

						char valor = aux_ladoA[i];
						aux_ladoA[i] = aux_ladoB[i];
						aux.push(aux_ladoB[i]);
						aux_ladoB[i] = valor;
						pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);
					}
					
					
					char value = aux_ladoA[0];
					aux_ladoA[0] = aux_ladoB[0];
					aux.push(aux_ladoB[0]);
					aux_ladoB[0] = value;
					pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);


				}
			}
		
		}

	}
	if (es_solucion(ladoB) == 1) {
		/*
		imprimir_lado(ladoA);
		printf("\n");
		imprimir_lado(ladoB);
		printf("\n");
		imprimir_pila(pila);
		printf("\n");
		return;
		//*/

	}
	imprimir_pila(pila);
	printf("\n");
	
}
