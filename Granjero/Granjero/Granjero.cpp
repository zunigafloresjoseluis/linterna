// Granjero.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>


using namespace std;

char ladoA[] = { 'G','R','L','O' };
char ladoB[] = { ' ',' ',' ',' ' };
void imprimir_lado(char lado[]);
void asignar(char aux_lado[], char lado[]);
void pasa_granjero(int profundidad, char ladoA[], char ladoB[], int lado,stack<char>pila);
int es_solucion(char lado[]);
int buscar(char a, char lado[]);
void imprimir_pila(stack<char> pila);
void no_pasados(char ladoA[], char ladoB[], char aux[]);
char granjero = 'G';
int main(){
	stack<char>pila;
	pasa_granjero(0, ladoA, ladoB, 0,pila);
    return 0;
}

void pasa_granjero(int profundidad, char ladoA[], char ladoB[], int lado, stack<char>pila) {
	if (profundidad <= 6) {
		if (es_solucion(ladoB) == 1) {
			///*
			imprimir_lado(ladoA);
			printf("\n");
			imprimir_lado(ladoB);
			printf("\n");
			imprimir_pila(pila);
			printf("\n");
			return;
			//*/

		}
		else {
			if (lado == 0) {

			//	for (int i = 0; i < 5; i++) {


					for (int j = 0; j < 5; j++) {
						char aux_ladoA[5];
						char aux_ladoB[5];
						asignar(aux_ladoA, ladoA);
						asignar(aux_ladoB, ladoB);
						//char ladoA[] = { '0-G','1-R','2-L','3-O' };
						if (aux_ladoA[j] != ' ') {
							stack<char>aux = pila;
							if ((ladoA[j] == 'R'&& ladoB[3] != 'O') || (ladoA[j] == 'R'&& ladoA[3] == ' ' &&ladoA[2] == ' ') || (ladoA[j] == 'R' && ladoA[0] == 'G')) {
								char val = aux_ladoB[0];
								aux_ladoB[0] = aux_ladoA[0];
								aux.push(aux_ladoA[0]);
								aux_ladoA[0] = val;

								char valor = aux_ladoB[j];
								aux_ladoB[j] = aux_ladoA[j];
								aux.push(aux_ladoA[j]);
								aux_ladoA[j] = valor;
								pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);
							}
							if ((ladoA[j] == 'L' && ladoB[3] != 'O') || (ladoA[j] == 'L' && ladoA[1] == ' ' && ladoA[3] == ' ')) {
								char val = aux_ladoB[0];
								aux_ladoB[0] = aux_ladoA[0];
								aux.push(aux_ladoA[0]);
								aux_ladoA[0] = val;

								char valor = aux_ladoB[j];
								aux_ladoB[j] = aux_ladoA[j];
								aux.push(aux_ladoA[j]);
								aux_ladoA[j] = valor;
								pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);

							}
							if ((ladoA[j] == 'O' && ladoB[1] != 'R' && ladoB[2] != 'L') || (ladoA[j] == 'O' &&ladoA[1] == ' ' && ladoA[2] == ' ')) {
								char val = aux_ladoB[0];
								aux_ladoB[0] = aux_ladoA[0];
								aux.push(aux_ladoA[0]);
								aux_ladoA[0] = val;

								char valor = aux_ladoB[j];
								aux_ladoB[j] = aux_ladoA[j];
								aux.push(aux_ladoA[j]);
								aux_ladoA[j] = valor;
								pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);

							}

							//pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 1, aux);
						}

					}





				//}



			}
			else {
				for (int i = 0; i < 5; i++) {
					char aux_ladoA[5];
					char aux_ladoB[5];
					asignar(aux_ladoA, ladoA);
					asignar(aux_ladoB, ladoB);
					stack<char>aux = pila;

					if (aux_ladoB[i] != ' ') {
						char value = aux_ladoA[0];
						aux_ladoA[0] = aux_ladoB[0];
						aux.push(aux_ladoB[0]);
						aux_ladoB[0] = value;
						if ((aux_ladoB[i] == 'R' && aux_ladoA[3] != 'O') || (aux_ladoB[i] == 'R' && aux_ladoB[3] == 'O')) {



							char valor = aux_ladoA[i];
							aux_ladoA[i] = aux_ladoB[i];
							aux.push(aux_ladoB[i]);
							aux_ladoB[i] = valor;
							pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);

						}
						if ((aux_ladoB[i] == 'L' && aux_ladoA[3] != 'O') || (aux_ladoB[i] == 'L' && aux_ladoB[3] == 'O')) {




							char valor = aux_ladoA[i];
							aux_ladoA[i] = aux_ladoB[i];
							aux.push(aux_ladoB[i]);
							aux_ladoB[i] = valor;
							pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);

						}
						if ((aux_ladoB[i] == 'O' && aux_ladoA[1] != 'R'&& aux_ladoA[2] != 'L') || (aux_ladoB[i] == 'L' && aux_ladoB[1] == 'R'&& aux_ladoB[2] == 'L')) {

							char valor = aux_ladoA[i];
							aux_ladoA[i] = aux_ladoB[i];
							aux.push(aux_ladoB[i]);
							aux_ladoB[i] = valor;
							pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);
						}




						pasa_granjero(profundidad + 1, aux_ladoA, aux_ladoB, 0, aux);


					}
				}

			}


		}




	}
}


void no_pasados(char ladoA[],char ladoB[],char aux[]) {
	for (int i = 0; i < 5; i++) {
		if (buscar(ladoB[i], ladoA) == 0) {
			aux[i] = ladoB[i];
		}
	
	}


}

void imprimir_pila(stack<char> pila) {
	stack<char>aux = pila;
	while (!aux.empty()) {
		printf("- %c", aux.top());
		aux.pop();	
	}
}


int es_solucion(char lado[]) {
	int contador = 0;
	
	for (int i = 0; i < 4; i++){
		if (buscar(lado[i],ladoA)==0) {
			contador++;
		}

	}
	if (contador > 0) {
		return 0;
	}
	return 1;

}

int buscar(char a,char lado[]) {
	for (int i = 0; i <4 ; i++){
		if (lado[i] == a) {
			return 1;
		}
	}

	return 0;
}
void asignar(char aux_lado[],char lado[]) {
	for (int i = 0; i < 4; i++) {
		aux_lado[i] = lado[i];
	}


}

void imprimir_lado(char lado[]) {
	for (int i = 0; i < 4; i++) {
		printf(" %c ", lado[i]);
	}
}



