// ranas_saltarinas.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stack>
using namespace std;
class rana {
private:
	int d0;
	int d1;
	int d2;
	int d3;
	int d4;
	int d5;
	int d6;
	
public:
	void setDato(int r[]) {
		d0 = r[0];
		d1 = r[1];
		d2 = r[2];
		d3 = r[3];
		d4 = r[4];
		d5 = r[5];
		d6 = r[6];
	}
	int getd0() { return d0; }
	int getd1() { return d1; }
	int getd2() { return d2; }
	int getd3() { return d3; }
	int getd4() { return d4; }
	int getd5() { return d5; }
	int getd6() { return d6; }
};

int solucionado(int estados_ranas[]);
void imprimir_ranas(int ranas[]);
void asignar(int uno[], int dos[]);
int saltos(int profundidad, int ranas[],int dato[]);
void imprimir_pila(stack<rana> pila);
int sapos[] = { 1,1,1,0,2,2,2 };

stack<rana> pila;



int main() {

	int a[] = { 1,1,1,0,2,2,2 };;
	

	saltos(0, sapos,a);
	
	return 1;
}


int solucionado(int estados_ranas[]) {
	int total_rojas = 0;
	int total_azules = 0;
	for (int i = 0; i < 7; i++) {
		if (i<3) {
			if (estados_ranas[i] == 2) {
				total_azules++;
			}
		}
		if (i > 3 && i < 7) {
			if (estados_ranas[i] == 1) {
				total_rojas++;
			}

		}

	}
	if (total_azules == 3 && total_rojas == 3) {
		return 1;
	}
	return 0;
}

int saltos(int profundidad,int ranas[],int dato[]) {
	
	
	if (profundidad < 30) {
		rana aux;
		aux.setDato(dato);
		pila.push(aux);
		if (solucionado(ranas) == 1) {
			imprimir_ranas(ranas);
			printf("solucion encontrada--->");
			printf("\n");
			imprimir_pila(pila);
			printf("\n fin \n ");
			return 1;
		}
		else {
		
			
			//printf("\n");
			//printf("\n profundidad %d",profundidad);
			for (int i = 0; i < 7; i++) {
				if (ranas[i] == 1) {

					if ((i+1)<7 && ranas[i + 1] == 0) {
						int aux_ranas[7];
						asignar(aux_ranas, ranas);
						
						int aux_valor = aux_ranas[i + 1];
						aux_ranas[i + 1] = aux_ranas[i];
						aux_ranas[i] = aux_valor;
						
						saltos(profundidad + 1, aux_ranas,aux_ranas);
					}
					if ((i+2)<7 &&  ranas[i + 1] == 2) {
						if ((i + 2) < 7) {
							if (ranas[i + 2] == 0) {
								int aux_ranas[7];
								asignar(aux_ranas, ranas);

								int aux_valor = aux_ranas[i + 2];

								aux_ranas[i + 2] = aux_ranas[i];
								aux_ranas[i] = aux_valor;
								

								saltos(profundidad + 1, aux_ranas,aux_ranas);
							}
						}
					}


				}
				if (ranas[i] == 2) {
					if (i != 0) {
						if (ranas[i - 1] == 0) {
							int aux_ranas[7];
							asignar(aux_ranas, ranas);


							int aux_valor = aux_ranas[i - 1];

							aux_ranas[i - 1] = aux_ranas[i];
							aux_ranas[i] = aux_valor;
							
							saltos(profundidad + 1, aux_ranas,aux_ranas);
						}
						if (ranas[i - 1] == 1) {//falta
							if (i - 2 > 0 && ranas[i-2]==0) {
								int aux_ranas[7];
								asignar(aux_ranas, ranas);

								int aux_valor = aux_ranas[i - 2];

								aux_ranas[i - 2] = aux_ranas[i];
								aux_ranas[i] = aux_valor;
								
								saltos(profundidad + 1, aux_ranas,aux_ranas);
							}
						}

					}
				}
				
			}
			pila.pop();
		}
			
		}
	


		
		

	
	return 1;
}


void asignar(int uno[], int dos[]) {
	for (int i = 0; i < 7; i++) {
		uno[i] = dos[i];
	}

}

void imprimir_ranas(int ranas[]) {
	for (int i = 0; i < 7; i++) {
		printf("%d  ", ranas[i]);
	}

}


void imprimir_pila(stack<rana> pila) {
	stack<rana>aux = pila;
	int tam = aux.size() - 1;
	while (!aux.empty()) {
		printf(" %d %d %d %d %d %d %d", aux.top().getd0(), aux.top().getd1(), aux.top().getd2(), aux.top().getd3(), aux.top().getd4(), aux.top().getd5(), aux.top().getd6());
		printf("\n");
		aux.pop();
	}
}